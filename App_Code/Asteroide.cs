﻿using System;

public class Asteroide
{
    // Definicion de variables...
    private string nombre;
    private double diametro;
    private string velocidad;
    private DateTime fecha;
    private string planeta;

    public Asteroide()
    {
        //Establecemos los valores por defecto...
        this.nombre = string.Empty;
        this.diametro = 0;
        this.velocidad = string.Empty;
        this.fecha = DateTime.Now;
        this.planeta = string.Empty;
    }

    // Constructor
    public Asteroide(string cNombreAux, double dDiametroAux, string cVelocidadAux, DateTime dFechaAux, string cPlanetaAux)
    {
        //Establecemos los valores por defecto...
        this.nombre = cNombreAux;
        this.diametro = dDiametroAux;
        this.velocidad = cVelocidadAux;
        this.fecha = dFechaAux;
        this.planeta = cPlanetaAux;
    }

    //Propiedades
    public string Nombre
    {
        get { return this.nombre; }
        set { this.nombre = value; }
    }

    public double Diametro
    {
        get { return this.diametro; }
        set { this.diametro = value; }
    }

    public string Velocidad
    {
        get { return this.velocidad; }
        set { this.velocidad = value; }
    }
    public DateTime Fecha
    {
        get { return this.fecha; }
        set { this.fecha = value; }
    }
    public string Planeta
    {
        get { return this.planeta; }
        set { this.planeta = value; }
    }
}