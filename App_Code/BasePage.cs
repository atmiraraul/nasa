﻿using System;
using System.Web;

public class BasePage : System.Web.UI.Page
{

    // Constructor
    // Add an event handler to Init event for the control
    // so we can execute code when a server control (page)
    // that inherits from this base class is initialized.
    public BasePage()
    {
        //Enlazamos el delegado del evento 'Init', con el procedimiento 'BasePage_Init'...
        this.Init += new EventHandler(BasePage_Init);
        this.Load += new EventHandler(BasePage_Load);
        this.Unload += new EventHandler(BasePage_Unload);
    }

    private void BasePage_Init(object sender, System.EventArgs e)
    {
    }

    private void BasePage_Unload(object sender, System.EventArgs e)
    {
    }

    private void BasePage_Load(object sender, System.EventArgs e)
    {
    }

    public static string getDomain()
    {
        string cDomain;
        cDomain = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
        return cDomain;
    }

    protected bool checkReferrer()
    {
        bool bTodoOk = true;
        string cDomain = getDomain();
        if (Request.ServerVariables["HTTP_REFERER"] != null)
        {
            Uri cReferrer = new Uri(Request.ServerVariables["HTTP_REFERER"].ToString());
            if (cDomain != cReferrer.Host)
            {
                bTodoOk = false;
            }
        }

        return bTodoOk;
    }

}