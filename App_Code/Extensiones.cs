﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.IO;
using System.Data;
using System.Drawing.Imaging;

namespace Medialabs
{
    /// <summary>
    /// La clase estática Extensiones contiene todas las extensiones de las clases nativas de .NET.
    /// </summary>
    public static class Extensiones
    {
        static Random rRandom = new Random(DateTime.Now.Millisecond);
        public const string VARIABLE_SESION_APLICACION = "";

        public enum eTipoSeleccion : int
        {
            TEXTO = 1,
            VALOR = 2
        }

        public enum eTipoTelefono : int
        {
            SIN_PREFIJO,
            PREFIJO,
            ANDORRA
        }

        #region "Extensiones de Procesamiento de Datos"
        #region "Clase String"
        public static int IsNull(this string sCadena, int iValorDefecto)
        {
            return (!String.IsNullOrWhiteSpace(sCadena) ? sCadena.ToInt32() : iValorDefecto);
        }

        public static DateTime IsNull(this string sCadena, DateTime dtValorDefecto)
        {
            return (!String.IsNullOrWhiteSpace(sCadena) ? sCadena.ToDateTime() : dtValorDefecto);
        }

        public static bool IsNull(this string sCadena, bool bValorDefecto)
        {
            return (!String.IsNullOrWhiteSpace(sCadena) ? sCadena.ToBoolean() : bValorDefecto);
        }

        public static Decimal IsNull(this string sCadena, Decimal dValorDefecto)
        {
            return (!String.IsNullOrWhiteSpace(sCadena) ? sCadena.ToDecimal() : dValorDefecto);
        }

        public static string IsNull(this string sCadena, string sValorDefecto)
        {
            return (!String.IsNullOrWhiteSpace(sCadena) ? sCadena : sValorDefecto);
        }

        public static int ToInt32(this string sCadena)
        {
            //Devolvemos la cadena convertida a entero...
            return Convert.ToInt32(sCadena);
        }

        public static short ToInt16(this string sCadena)
        {
            //Devolvemos la cadena convertida a entero...
            return Convert.ToInt16(sCadena);
        }

        public static bool ToBoolean(this string sCadena)
        {
            bool bResultado = false;

            //Comprobamos si la cadena NO es nula...
            if (!string.IsNullOrWhiteSpace(sCadena))
            {
                //Comprobamos si la cadena se trata de 1 | 0...
                if (sCadena == "1" || sCadena == "0")
                {
                    //Establecemos el resultado acorde a la conversión de 'bits'...
                    bResultado = (sCadena == "1" ? true : false);
                }
                else if (sCadena.ToUpper().eliminarAcentos() == "SI" || sCadena.ToUpper().eliminarAcentos() == "NO")
                {
                    //Establecemos el resultado acorde a la conversión de SI = True y NO = False
                    bResultado = (sCadena.ToUpper() == "SI" ? true : false);
                }
                else
                {
                    //Establecemos el resultado acorde a la conversión...
                    bResultado = Convert.ToBoolean(sCadena);
                }
            }

            //Devolvemos la cadena convertida a booleano...
            return bResultado;
        }

        public static DateTime ToDateTime(this string sCadena)
        {
            //Devolvemos la cadena convertida a fecha...
            return Convert.ToDateTime(sCadena);
        }

        public static bool IsDateTime(this string sCadena)
        {
            bool bResultado = true;
            DateTime tempDate;

            try
            {
                //Intentamos convertir la cadena sFecha, en un datetime...
                tempDate = Convert.ToDateTime(sCadena);
            }
            catch
            {
                //Establecemos el resultado como falso...
                bResultado = false;
            }

            //Devolvemos el resultado...
            return bResultado;
        }

        public static Decimal ToDecimal(this string sCadena) { 
            //Devolvemos la cadena convertida a decimal...
            return sCadena.ToDecimal(-1);
        }

        public static Decimal ToDecimal(this string sCadena, int iNumeroDecimales)
        {
            Decimal dResultado = 0.00M;

            //Realizamos la conversión...
            dResultado = Convert.ToDecimal(sCadena);

            //Comprobamos si se han especificado números decimales...
            if (iNumeroDecimales > 0) { 
                //Realizamos el redondeo...
                dResultado = Decimal.Round(dResultado, iNumeroDecimales);
            }

            //Devolvemos la cadena convertida a decimal...
            return dResultado;
        }

        public static float ToFloat(this string sCadena)
        {
            float fResultado = 0.00F;

            //Validamos que sea un float válido...
            if (validarFloat(sCadena))
            {
                //Realizamos la conversion a Float...
                fResultado = float.Parse(sCadena);
            }

            //Devolvemos la cadena convertida a float...
            return fResultado;
        }

        public static string ToDBValue(this string sCadena)
        {
            //Devolvemos el resultado dependiendo de, si la cadena es nula, o bien, se trata de una cadena vacía...
            return (!String.IsNullOrWhiteSpace(sCadena) ? sCadena : "NULL");
        }

        public static string ToDBValue(this int iNumero)
        {
            //Devolvemos el resultado dependiendo de, si la cadena es nula, o bien, se trata de una cadena vacía...
            return (iNumero != -1 ? iNumero.ToString() : "NULL");
        }

        public static string ToDBValue(this DateTime dtCadena)
        {
            //Devolvemos el resultado dependiendo de, si la cadena es nula, o bien, se trata de una cadena vacía...
            return (dtCadena != DateTime.MinValue ? dtCadena.ToString() : "NULL");
        }

        public static string eliminarCaracteresInvalidos(this string sCadena, bool bEliminarPunto = false)
        {
            return (!String.IsNullOrWhiteSpace(sCadena) ? sCadena.Formatear(bEliminarPunto).eliminarAcentos() : string.Empty);
        }

        public static string Formatear(this string sCadena, bool bEliminarPunto = false)
        {
            Dictionary<List<string>, string> dCaracteresInvalidos = new Dictionary<List<string>, string>() { 
                                            { new List<string>() { "%20", "&nbsp;", " ", "-" }, "_" }, 
                                            { new List<string>() {"'", "!", "`", "´", "¡", "º", "ç", "Ç", "ª", "$", "&", "\"", "/", "·", "#", "¨", ":", ";", ",", "¬", "€", "%", "+", "*", "<", ">", "[", "]", "(", ")", "{", "}", "=", "?", "¿", "^", "|", "\\", "@", "~"}, string.Empty }};

            //Recorremos el diccionario (formato Clave/Valor)...
            foreach (KeyValuePair<List<string>, string> kvpValores in dCaracteresInvalidos)
            {
                //Recorremos cada una de los elementos almacenados dentro de la clave del diccionario (ya que, en éste caso, es una lista...)
                foreach (string sClave in kvpValores.Key)
                {
                    //Sustituimos, en la cadena original, los carácteres 'sClave' por los carácteres almacenados en el valor del diccionario (.Value)
                    sCadena = sCadena.Replace(sClave, kvpValores.Value);
                }
            }

            //Limpiamos los últimos carácteres especiales...
            sCadena = sCadena.Replace("ñ", "n");
            sCadena = sCadena.Replace("Ñ", "N");

            //Finalmente, comprobamos si es necesario eliminar el punto...
            sCadena = (bEliminarPunto == true ? sCadena.Replace(".", string.Empty) : sCadena);

            //Devolvemos el resultado...
            return sCadena;
        }

        public static string eliminarAcentos(this string sCadena)
        {
            Encoding destEncoding = Encoding.GetEncoding("iso-8859-8");

            return destEncoding.GetString(Encoding.Convert(Encoding.UTF8, destEncoding, Encoding.UTF8.GetBytes(sCadena)));
        }

        public static string Recortar(this string sCadena, int iLongitudMaxima, bool bCadenaFinal = true, string sCadenaFinal = "...")
        {
            string sResultado = string.Empty;

            //Comprobamos si la cadena sobre la que se está ejecutando el método, presenta valor...
            if (!string.IsNullOrWhiteSpace(sCadena))
            {
                //Comprobamos si NO se va a utilizar cadena final (en caso contrario, sencillamente se recortaría...)
                if (!bCadenaFinal)
                {
                    //Limpiamos la cadena final...
                    sCadenaFinal = string.Empty;
                }

                //Establecemos como resultado la cadena...
                sResultado = sCadena;

                //Comprobamos si la cadena es mayor que la longitud máxima pasada por parámetro...
                if (sCadena.Length > iLongitudMaxima)
                {
                    //Recortamos la cadena, y concatenamos la cadena final, en caso de que fuese necesario...
                    sResultado = String.Concat(sCadena.Substring(0, iLongitudMaxima - sCadenaFinal.Length), sCadenaFinal);
                }
            }

            //Devolvemos el resultado...
            return sResultado;
        }

        public static string Resaltar(this string sCampoCompleto, string sCadenaBusqueda, int iLongitudExtraccion)
        {
            string sResultado = string.Empty;
            string sPreResultado = string.Empty;
            string sPostResultado = string.Empty;
            int iPosicionCadenaBusqueda = -1;

            //Recuperamos la posición de la cadena de busqueda dentro del campo...
            iPosicionCadenaBusqueda = sCampoCompleto.eliminarAcentos().IndexOf(sCadenaBusqueda.eliminarAcentos(), StringComparison.OrdinalIgnoreCase);

            //Comprobamos si NO existe ningún problema para recuperar los 40 carácteres anteriores/posteriores a la posición + length de la cadena de búsqueda...
            if (((iPosicionCadenaBusqueda - iLongitudExtraccion) > 0) && (((iPosicionCadenaBusqueda + sCadenaBusqueda.Length + iLongitudExtraccion) < sCampoCompleto.Length)))
            {
                //Recuperamos los iLongitudExtraccion carácteres anteriores al token de búsqueda...
                sPreResultado = sCampoCompleto.Substring(iPosicionCadenaBusqueda - iLongitudExtraccion, iLongitudExtraccion);

                //Recuperamos los iLongitudExtraccion carácteres posteriores al token de búsqueda...
                sPostResultado = sCampoCompleto.Substring(iPosicionCadenaBusqueda + sCadenaBusqueda.Length, iLongitudExtraccion);
            }
            else
            {
                //En éste caso, no se cumple de forma integra la condición de extraer los 40 carácteres anteriores/posteriores al token de búsqueda...
                //Procedemos a realizar la extracción del contenido...

                //Comprobamos si se pueden extraer los iLongitudExtraccion carácteres previos al token...
                if ((iPosicionCadenaBusqueda - iLongitudExtraccion) > 0)
                {
                    //Recuperamos los 40 carácteres anteriores al token de búsqueda...
                    sPreResultado = sCampoCompleto.Substring(iPosicionCadenaBusqueda - iLongitudExtraccion, iLongitudExtraccion);
                }
                else
                {
                    //Recuperamos los 40 carácteres anteriores al token de búsqueda...
                    sPreResultado = sCampoCompleto.Substring(0, iPosicionCadenaBusqueda);
                }

                //Comprobamos si se pueden extraer los iLongitudExtraccion carácteres previos al token...
                if ((iPosicionCadenaBusqueda + sCadenaBusqueda.Length + iLongitudExtraccion) < sCampoCompleto.Length)
                {
                    //Recuperamos los 40 carácteres posteriores al token de búsqueda...
                    sPostResultado = sCampoCompleto.Substring(iPosicionCadenaBusqueda + sCadenaBusqueda.Length, iLongitudExtraccion);
                }
                else
                {
                    //Recuperamos los iLongitudExtraccion carácteres posteriores al token de búsqueda...
                    sPostResultado = sCampoCompleto.Substring(iPosicionCadenaBusqueda + sCadenaBusqueda.Length);
                }
            }

            //Establecemos el resultado...
            sResultado = String.Concat("...", sPreResultado, "<B>", sCadenaBusqueda, "</B>", sPostResultado, "...");

            //Devolvemos el resultado...
            return sResultado;        
        }

        static public string Capitalizar(this string sCadena)
        {
            string[] sCadenaTrozeada;
            string sResultado = string.Empty;
            StringBuilder sbResultado = new StringBuilder();

            //Comprobamos si el parámetro sCadena presenta contenido, y si además, este no es nulo...
            if (!String.IsNullOrEmpty(sCadena))
            {
                //Trozeamos la cadena inicial, utilizando como patrón, el espacio en blanco...
                sCadenaTrozeada = sCadena.Split(' ');

                //Comprobamos si se ha recuperado algún elemento...
                if (sCadenaTrozeada.Length > 0)
                {
                    //Recorremos cada uno de los elementos que han sido trozeados...
                    foreach (string sTrozo in sCadenaTrozeada)
                    {
                        //Comprobamos si la cadena trozeada presenta contenido...
                        if (String.IsNullOrEmpty(sTrozo) == false)
                        {
                            //Comprobamos si la cadena, presenta una longitud mayor de 1, o bien, es una de las primeras palabras de la cadena...
                            if (sTrozo.Length > 1 || ( sCadena.IndexOf(sTrozo) <= 2 ) )
                            {
                                //Construimos la cadena de nuevo, generando el primer carácter en mayúscula, y el resto en minúscula...
                                sbResultado.Append(sTrozo[0].ToString().ToUpper());
                                sbResultado.Append(sTrozo.Substring(1).ToLower());
                            }
                            else {
                                //En éste caso, el carácter será en mayúscula...
                                sbResultado.Append(sTrozo[0].ToString().ToLower());
                            }
                        }

                        //Concatenamos el espacio...
                        sbResultado.Append(" ");
                    }

                    //Eliminamos el último espacio final...
                    sResultado = sbResultado.ToString().Substring(0, sbResultado.ToString().Length - 1);
                }
            }

            return sResultado; //sbResultado.ToString();            
        }

        static public string getMD5(this string sCadena)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(sCadena));
            for (int i = 0; i < stream.Length; i++)
                sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        static public string obtenerNombreFichero(this string sCadena)
        {
            string sResultado = string.Empty;

            //Comprobamos si NO hemos recibido una cadena nula...
            if (!String.IsNullOrWhiteSpace(sCadena))
            {
                //En éste caso, extraemos el nombre del fichero...
                sResultado = sCadena.Substring(sCadena.LastIndexOf('/') + 1);
            }

            //Devolvemos el resultado...
            return sResultado;
        }

        static bool esFichero(this string sCadena)
        {
            //Devolvemos el resultado a través del método exists de la clase FileInfo (sólo para procesamiento de ficheros)...
            return new FileInfo(sCadena).Exists;
        }

        static bool esCarpeta(this string sCadena)
        {
            //Devolvemos el resultado a través del método exists de la clase DirectoryInfo (sólo para procesamiento de ficheros)...
            return new DirectoryInfo(sCadena).Exists;
        }

        static public string eliminarHtml(this string sCadena)
        {
            //Devolvemos la cadena sin Html...
            return Regex.Replace(sCadena, @"<[^>]*>", string.Empty);
        }

        static public bool caracteresErroneosEmail(this string sEmail)
        {
            bool bResultado = false;
            List<string> lstCaracteresErroneos = new List<string>() { "ñ", "ñ", "á", "à", "â", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "ö", "ú", "ù", "û", "ü", ".@" };

            //Recorremos cada uno de los carácteres erróneos...
            foreach (string sCaracter in lstCaracteresErroneos)
            {
                //Comprobamos si el Email contiene el carácter que está siendo recorrido...
                if (sEmail.ToLower().IndexOf(sCaracter.ToLower()) != -1)
                {
                    //Fallamos la validación...
                    bResultado = true;
                }

                //Comprobamos si ha fallado la validación...
                if (bResultado)
                {
                    //Salimos del bucle...
                    break;
                }
            }

            //Devolvemos el resultado...
            return bResultado;
        }

        static public string recuperar_variable(this HttpContext context, string nombre_variable)
        {
            string resultado = string.Empty;

            //Comprobamos si existe la variable 'nombre_variable' dentro de las claves del formulario...
            if (context.Request.Form[nombre_variable] != null && !String.IsNullOrWhiteSpace(context.Request.Form[nombre_variable]))
            {
                //Recuperamos el valor de la variable 'nombre_variable'...
                resultado = context.Request.Form[nombre_variable].ToString();
            }

            //Devolvemos el resultado...
            return resultado;
        }

        static public string parsearGenero(this string sCadena)
        {
            string sResultado = string.Empty;

            //Comprobamos el valor para parsearlo...
            switch (sCadena.ToLower())
            {
                case "male":
                    sResultado = "H";
                    break;
                case "female":
                    sResultado = "M";
                    break;
            }

            //Devolvemos la cadena...
            return sResultado;
        }

        static public string aCadenaBoolean(this string bCadena)
        {
            string sResultado = string.Empty;

            if (bCadena.ToLower() == "true")
                sResultado = "Sí";
            else
                sResultado = "No";

                //Devolvemos la cadena...
                return sResultado;
        }
        #endregion

        #region "Clase DateTime"
        public static string IsNull(this DateTime dtFecha, string sValorDefecto)
        {
            return (!(dtFecha == DateTime.MinValue || String.IsNullOrWhiteSpace(dtFecha.ToString())) ? dtFecha.ToString() : sValorDefecto); 
        }
        #endregion

        #region "Clase Object"
        static public string obtenerRequest(this object objeto, string sRequest)
        {
            string sResultado = string.Empty;

            //Comprobamos si se ha recibido algún parámetro...
            if (HttpContext.Current.Request.QueryString != null && HttpContext.Current.Request.QueryString.Count > 0)
            {
                //Comprobamos si se ha recuperado el parámetro 'problemasAcceso'...
                if (HttpContext.Current.Request.QueryString[sRequest] != null && !String.IsNullOrWhiteSpace(HttpContext.Current.Request.QueryString[sRequest].ToString()))
                {
                    //Recuperamos el valor del QueryString pasado por parámetro...
                    sResultado = HttpContext.Current.Request.QueryString[sRequest].ToString();
                }
            }

            //Devolvemos el resultado...
            return sResultado;
        }

        static public T[] crearInstancia<T>(int arrayLength) where T : new() {
            //Llamamos a la sobrecarga del método...
            return new object().crearInstancia<T>(arrayLength);
        }

        static public T[] crearInstancia<T>(this object objeto, int arrayLength) where T: new()
        {
            T[] result = new T[arrayLength];
            for (int i=0; i<arrayLength; i++)
               result[i] = new T();
            return result;
        }
        #endregion

        #region "Clase NameValueCollection"
        static public Dictionary<string, string> ToDictionary(this NameValueCollection formulario)
        {
            Dictionary<string, string> resultado = new Dictionary<string, string>();
            foreach (string key in formulario.AllKeys)
                resultado.Add(key, formulario[key]);

            //Devolvemos el resultado...
            return resultado;
        }
        #endregion

        #region "Enumerados"
        public static bool IsNullOrEmpty(this Decimal dDecimal) {
            bool bResultado = false;

            //Comprobamos si el decimal pasado por parámetro, se trata de una cadena vacía, o bien, si NO presenta contenido...
            if (dDecimal == null || dDecimal == 0.0M || String.IsNullOrWhiteSpace(dDecimal.ToString())) { 
                //Establecemos el resultado como verdadero...
                bResultado = true;
            }

            //Devolvemos el resultado...
            return bResultado;
        }
        #endregion

        #region "Web Controls"
        static public void establecerTexto(this LinkButton LBControl, string sCadena)
        {
            //Comprobamos si se ha pasado alguna cadena válida como parámetro...
            if (!string.IsNullOrWhiteSpace(sCadena))
            {
                //Comprobamos si el LinkButton presenta controles...
                if (LBControl.HasControls())
                {
                    //Recorremos los controles internos del linkbutton...
                    foreach (Control cControlAux in LBControl.Controls)
                    {
                        //Comprobamos si el control es de tipo LiteralControl...
                        if (cControlAux.GetType() == typeof(System.Web.UI.LiteralControl))
                        {
                            //Establecemos el texto pasado por parámetro...
                            ((LiteralControl)cControlAux).Text = sCadena;
                        }
                    }
                }
            }
        }

        static public void eliminarImagen(this LinkButton LBControl)
        {
            //Comprobamos si el LinkButton presenta controles...
            if (LBControl.HasControls())
            {
                //Recorremos los controles internos del linkbutton...
                foreach (Control cControlAux in LBControl.Controls)
                {
                    //Comprobamos si el control es de tipo LiteralControl...
                    if (cControlAux.GetType() == typeof(System.Web.UI.WebControls.Image))
                    {
                        //Ocultamos la imágen del LinkButton...
                        cControlAux.Visible = false;
                    }
                }
            }
        }

        static public void capitalizarElementos(this DropDownList DDLControl)
        {
            //Comprobamos si el desplegable contiene Elementos...
            if (DDLControl.Items != null && DDLControl.Items.Count > 0)
            {
                //Recorremos cada elemento dentro de los elementos del desplegable...
                foreach (ListItem liElemento in DDLControl.Items)
                {
                    //Capitalizamos el texto del elemento...
                    liElemento.Text = liElemento.Text.Capitalizar();
                }
            }
        }

        static public void establecerColorTexto(this Control cControl, string sColor)
        {
            //Comprobamos el tipo de control...
            if (cControl is Label)
            {
                //Establecemos el color al Label...
                ((Label)cControl).ForeColor = System.Drawing.ColorTranslator.FromHtml(sColor);
            }
            else if (cControl is LinkButton)
            {
                //Establecemos el color al LinkButton...
                ((LinkButton)cControl).ForeColor = System.Drawing.ColorTranslator.FromHtml(sColor);
            }
            else if (cControl is HyperLink)
            {
                //Establecemos el color al HyperLink...
                ((HyperLink)cControl).ForeColor = System.Drawing.ColorTranslator.FromHtml(sColor);
            }
        }

        static public void seleccionarElemento(this DropDownList DDLControl, string sElemento)
        {
            //Llamamos a la sobrecarga del método...
            seleccionarElemento(DDLControl, sElemento, eTipoSeleccion.VALOR);
        }

        static public void seleccionarElemento(this DropDownList DDLControl, bool bElemento)
        {
            //Llamamos a la sobrecarga del método...
            seleccionarElemento(DDLControl, bElemento.ToString(), eTipoSeleccion.VALOR);
        }

        static public void seleccionarElemento(this DropDownList DDLControl, int iElemento)
        {
            //Llamamos a la sobrecarga del método...
            seleccionarElemento(DDLControl, iElemento.ToString(), eTipoSeleccion.VALOR);
        }

        static public void seleccionarElemento(this DropDownList DDLControl, ListItem liElemento) { 
            //Comprobamos si el DropDownList dispone de elementos...
            if (DDLControl.Items.Count > 0)
            {
                //Comprobamos si actualmente, existe algún elemento seleccionado en el desplegable...
                if (DDLControl.SelectedItem != null)
                {
                    //Deseleccionamos el elemento...
                    DDLControl.SelectedItem.Selected = false;
                }

                //Seleccionamos el elemento que se recuperó previamente...
                liElemento.Selected = true;
            }
        }

        static public void seleccionarElemento(this DropDownList DDLControl, string sElemento, eTipoSeleccion tsTipoSeleccion)
        {
            ListItem liAux = null;

            //Comprobamos si el DropDownList dispone de elementos...
            if (DDLControl.Items.Count > 0)
            {
                //Comprobamos el tipo de selección...
                if (tsTipoSeleccion == eTipoSeleccion.TEXTO)
                {
                    //Recuperamos el elemento según el texto...
                    liAux = DDLControl.Items.FindByText(sElemento);
                }
                else
                {
                    //Comprobamos si el elemento pasado por parámetro se corresponde con un booleano...
                    if (sElemento.ToLower() == "true" || sElemento.ToLower() == "false")
                    {
                        //Recuperamos el elemento según el valor...
                        liAux = DDLControl.Items.FindByValue((sElemento.ToLower() == "true" ? "1" : "0"));
                    }
                    else {
                        //Recuperamos el elemento según el valor...
                        liAux = DDLControl.Items.FindByValue(sElemento);                    
                    }
                }

                //Comprobamos si se ha recuperado algún elemento...
                if (liAux != null)
                {
                    //Comprobamos si actualmente, existe algún elemento seleccionado en el desplegable...
                    if (DDLControl.SelectedItem != null)
                    {
                        //Deseleccionamos el elemento...
                        DDLControl.SelectedItem.Selected = false;
                    }

                    //Seleccionamos el elemento que se recuperó previamente...
                    liAux.Selected = true;
                }
            }
        }
        #endregion
        #endregion

        #region "Extensiones de Validación"
        #region "Clase String"
        static public bool validarEmail(this string sCadena)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex re = new Regex(strRegex);

            //Devolvemos el resultado...
            return re.IsMatch(sCadena);
        }

        static public bool validarCaracteresErroneosEmail(this string sEmail)
        {
            bool bResultado = true;
            List<string> lstCaracteresErroneos = new List<string>() { "ñ", "ñ", "á", "à", "â", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "ö", "ú", "ù", "û", "ü", ".@" };

            //Recorremos cada uno de los carácteres erróneos...
            foreach (string sCaracter in lstCaracteresErroneos)
            {
                //Comprobamos si el Email contiene el carácter que está siendo recorrido...
                if (sEmail.ToLower().IndexOf(sCaracter.ToLower()) != -1)
                {
                    //Fallamos la validación...
                    bResultado = false;
                }

                //Comprobamos si ha fallado la validación...
                if (!bResultado)
                {
                    //Salimos del bucle...
                    break;
                }
            }

            //Devolvemos el resultado...
            return bResultado;
        }

        static public bool validarFecha(this string sCadena)
        {
            bool bResultado = true;
            DateTime tempDate;

            try
            {
                //Intentamos convertir la cadena sFecha, en un datetime...
                tempDate = Convert.ToDateTime(sCadena);
            }
            catch
            {
                //Establecemos el resultado como falso...
                bResultado = false;
            }

            //Devolvemos el resultado...
            return bResultado;
        }

        static public bool validarFloat(this string sCadena)
        {
            float tempFloat;

            //Devolvemos el resultado...
            return float.TryParse(sCadena, out tempFloat);
        }

        static public bool validarNumerico(this string sCadena)
        {
            Regex numericRegex = new Regex(@"^\d+$");
            Match m = numericRegex.Match(sCadena);

            //Devolvemos el resultado...
            return m.Success;
        }

        static public bool validarHTML(this string sCadena)
        {
            Regex htmlRegex = new Regex("<(\"[^\"]*\"|'[^']*'|[^'\">])*>");
            Match m = htmlRegex.Match(sCadena);

            return !m.Success;
        }

        static public bool validarUrlYoutube(this string sCadena)
        {
            bool bResultado = false;
            string sIdVideo = string.Empty;
            Uri uURI = null;

            //Comprobamos si la cadena pasada por parámetro, no se encuentra vacía o es nula...
            if (!string.IsNullOrEmpty(sCadena))
            {
                //Comprobamos si la URL contiene el protocolo HTTP...
                if (sCadena.ToUpper().IndexOf("HTTP://") == -1)
                {
                    //Establecemos el protocolo HTTP en la URL...
                    sCadena = string.Concat("http://", sCadena);
                }

                try
                {
                    //Instanciamos un objeto de tipo URI, a través de la URL pasada por parámetro...
                    uURI = new Uri(sCadena);
                }
                catch (UriFormatException e)
                {

                }

                //Comprobamos si se ha recuperado la URI..
                if (uURI != null)
                {
                    //Parseamos el QueryString...
                    sIdVideo = HttpUtility.ParseQueryString(uURI.Query).Get("v");

                    //Comprobamos si se ha recuperado el parámetro 'v', éste contiene valor, y además, cumple el formato exigido...
                    if (!String.IsNullOrEmpty(sIdVideo) && sIdVideo.Length == 11)
                    {
                        //Establecemos el resultado a verdadero...
                        bResultado = true;
                    }
                }
            }

            //Devolvemos el resultado...
            return bResultado;
        }

        static public bool validarUrlFlickr(this string sCadena)
        {
            bool bResultado = false;
            string sIdGaleria = string.Empty;
            string[] sListaSeccionesURL = null;
            Uri uURI = null;

            //Comprobamos si la cadena pasada por parámetro, no se encuentra vacía o es nula...
            if (!string.IsNullOrEmpty(sCadena))
            {
                //Comprobamos si la URL contiene el protocolo HTTP...
                if (sCadena.ToUpper().IndexOf("HTTP://") == -1)
                {
                    //Establecemos el protocolo HTTP en la URL...
                    sCadena = string.Concat("http://", sCadena);
                }

                try
                {
                    //Instanciamos un objeto de tipo URI, a través de la URL pasada por parámetro...
                    uURI = new Uri(sCadena);
                }
                catch (UriFormatException e)
                {

                }

                //Comprobamos si se ha recuperado la URI..
                if (uURI != null)
                {
                    //Limpiamos de espacio en blanco la URL...
                    sCadena = sCadena.Trim();

                    //Primero, comprobamos si el último carácter de la URL se trata de '/'...
                    if (sCadena[sCadena.Length - 1] == '/')
                    {
                        //En éste caso, haríamos un substring desde el inicio hasta la última posición -1 (Así nos ahorramos una excepción en la validación, ya que la última dimensión del array, sería '', por corresponder con el token último de la URL '/'...
                        sCadena = sCadena.Substring(0, sCadena.Length - 1);
                    }

                    //Trozeamos la lista de URL's, utilizando como token, "/"...
                    sListaSeccionesURL = sCadena.Split('/');

                    //Comprobamos si se ha separado correctamente las URL's...
                    if (sListaSeccionesURL.Length > 0)
                    {
                        //En éste caso, accedemos a la última dimensión...
                        sIdGaleria = sListaSeccionesURL[sListaSeccionesURL.Length - 1].ToString();

                        //Comprobamos si el ID_GALERIA_FLICKR, cumple con el formato...
                        if (sIdGaleria.Length == 17)
                        {
                            //Establecemos el resultado a verdadero...
                            bResultado = true;
                        }
                    }
                }
            }

            //Devolvemos el resultado...
            return bResultado;
        }

        static public bool validarUrl(this string sCadena)
        {
            string strRegex = "^(https?://)"
            + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //user@
            + @"(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP- 199.194.52.184
            + "|" // allows either IP or domain
            + @"([0-9a-z_!~*'()-]+\.)*" // tertiary domain(s)- www.
            + @"([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." // second level domain
            + "[a-z]{2,6})" // first level domain- .com or .museum
            + "(:[0-9]{1,4})?" // port number- :80
            + "((/?)|" // a slash isn't required if there is no file name
            + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
            Regex re = new Regex(strRegex);

            //Devolvemos el resultado...
            return re.IsMatch(sCadena);
        }

        static public bool validarTelefono(this string sTelefono, eTipoTelefono ttTipoTelefono)
        {
            string sTipoCampo = string.Empty;
            int iLongitudCampo = 9;
            bool bResultado = false;

            //Comprobamos, si se ha activado el prefijo...
            if (ttTipoTelefono == eTipoTelefono.PREFIJO)
            {
                iLongitudCampo += 4;
            }
            else if (ttTipoTelefono == eTipoTelefono.ANDORRA)
            {
                iLongitudCampo = 6;
            }

            //Comprobamos si, la longitud del campo es la misma que la del teléfono...
            if ((ttTipoTelefono == eTipoTelefono.ANDORRA && sTelefono.validarNumerico()) || (!(iLongitudCampo > sTelefono.Length) && sTelefono.validarNumerico()))
            {
                bResultado = true;
            }

            //Devolvemos el resultado...
            return bResultado;
        }
        #endregion
        #endregion

        public static Stream ToStream(this System.Drawing.Image image, ImageFormat formaw)
        {
            var stream = new System.IO.MemoryStream();
            image.Save(stream, formaw);
            stream.Position = 0;
            return stream;
        }
    }
}