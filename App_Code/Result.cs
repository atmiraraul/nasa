﻿using System.Collections.Generic;
using System.Web.Script.Serialization;

public class Result
{
    // Definicion de variables miembro 
    private int coderesult;
    private string message;
    private List<Asteroide> lstasterioides;

    // Constructor
    public Result()
    {
        //Establecemos los valores por defecto...
        this.coderesult = 0;
        this.message = string.Empty;
        this.lstasterioides = new List<Asteroide>();
    }

    public Result(int nCodeResultAux, string cMessageAux)
    {
        //Establecemos los valores...
        this.coderesult = nCodeResultAux;
        this.message = cMessageAux;
        this.lstasterioides = new List<Asteroide>();
    }

    public Result(int nCodeResultAux, string cMessageAux, List<Asteroide> lstAsterioidesAux)
    {
        //Establecemos los valores...
        this.coderesult = nCodeResultAux;
        this.message = cMessageAux;
       this.lstasterioides = lstAsterioidesAux;
    }

    public int nCodeResult
    {
        get { return this.coderesult; }
        set { this.coderesult = value; }
    }

    public string cMessage
    {
        get { return this.message; }
        set { this.message = value; }
    }

    public List<Asteroide> lstAsterioides
    {
        get { return this.lstasterioides; }
        set { this.lstasterioides = value; }
    }

    // Propiedad de solo lectura
    public string toJson(Result cResult)
    {
        var jsonStringName = new JavaScriptSerializer();
        var jsonStringResult = jsonStringName.Serialize(cResult);
            
        //Devolvemos el JSON...
        return jsonStringResult;
    }
}