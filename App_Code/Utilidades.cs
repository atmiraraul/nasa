using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;

/// <summary>
/// Clase estática encargada de procesar diversa información,...
/// </summary>
static public class Utilidades
{
    static public List<string> lstPlatenasDisponibles = new List<string>() { "earth", "juptr", "venus", "merc", "mars", "moon" };

    static public string nombreServidor()
    {
        string sResultado;

        //Obtenemos el nombre del servidor...
        sResultado = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];

        //Devolvemos el resultado...
        return sResultado;
    }

    static public string nombreDominio()
    {
        string sResultado;

        //Obtenemos el nombre del servidor...
        sResultado = string.Concat(Utilidades.protocoloServidor(), HttpContext.Current.Request.Url.Host, (HttpContext.Current.Request.Url.IsDefaultPort ? string.Empty : ":" + HttpContext.Current.Request.Url.Port), "/");//, "/recetas2017/");

        //Devolvemos el resultado...
        return sResultado;
    }

    static public string protocoloServidor()
    {
        return (Utilidades.servidorDesarrollo() ? "http://" : "https://");
    }

    static public bool servidorDesarrollo()
    {
        bool bResultado = true;
        string sURL = string.Empty;

        //Comprobamos si nos encontramos en el servidor de desarrollo...
        sURL = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];

        //Comprobamos si nos encontramos en el servidor de desarrollo...
        if (sURL.IndexOf("localhost") == -1)
        {
            //Marcamos la bandera de servidor desarrollo a falso...
            bResultado = false;
        }

        return bResultado;
    }

    static public String obtenerQueryString(string cVariable, string cValorDefecto)
    {
        string cValor = cValorDefecto;
        //Recuperamos los valores...
        if (HttpContext.Current.Request.QueryString[cVariable] != null && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[cVariable]))
        {
            //Recuperamos el valor...
            cValor = HttpContext.Current.Request.QueryString[cVariable].Trim();
        }

        //Devolvemos el valor...
        return cValor;
    }

}