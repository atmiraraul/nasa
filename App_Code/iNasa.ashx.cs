﻿using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

public class iNasa : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public string cPlanet = string.Empty;

    public void ProcessRequest(HttpContext context)
    {
        Result cResult = new Result();

        try
        {
            //Recuperamos los parametros...
            cPlanet = Utilidades.obtenerQueryString("planet", "");
            
            //Realizamos la validación...
            if (validarProceso(ref cResult))
            {
                //Recuperamos los asteroides...
                recuperarAsteroides(ref cResult);
            }
        }
        catch (Exception ex)
        {
            //Establecemos el mensaje de alerta...
            cResult = new Result(-2, "En estos momentos no está disponible la información solicitada, inténtelo de nuevo pasados unos minutos.");
        }

        //Establecemos las cabeceras...
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/json; charset=utf-8";
        HttpContext.Current.Response.Write(cResult.toJson(cResult));
    }

    public bool validarProceso(ref Result cResult)
    {
        //Comprobamos si el parámetro está vacío...
        if (string.IsNullOrEmpty(cPlanet))
        {
            //Establecemos el mensaje de alerta...
            cResult = new Result(-1, "Debe incluir el parámetro 'planet'.");
        }
        else if (!Utilidades.lstPlatenasDisponibles.Contains(cPlanet.ToString().ToLower()))
        {
            //Establecemos el mensaje de alerta...
            cResult = new Result(-1, "Planeta no identificado.");
        }

        //Devolvemos el resultado...
        return (cResult.nCodeResult == 0);
    }

    public void recuperarAsteroides(ref Result cResult)
    {
        Dictionary<string, object> result = new Dictionary<string, object>();
        List<Asteroide> LstAsteroides = new List<Asteroide>();

        //Realizamos la primera petición...
        JObject joResponse = obtenerDatosPeticion(ConfigurationManager.AppSettings["URL_PETICION"].ToString().Replace("#API_KEY#", ConfigurationManager.AppSettings["API_KEY"].ToString()));

        //Recorremos la lista hasta el final...
        while (joResponse != null)
        {
            //Recuperamos el objeto asteroide...
            JArray aAsteroides = (JArray)joResponse["near_earth_objects"];

            //Recorremos los asteroides...
            foreach (var asteroide in aAsteroides)
            {
                //Comprobamos si es un asteroide peligroso...
                if (Convert.ToBoolean(asteroide["is_potentially_hazardous_asteroid"].ToString()))
                {
                    //Recorremos las aproximaciones de los planetas...
                    foreach (var approach_data in asteroide["close_approach_data"])
                    {

                        //Código utilizado para obtener la lista completa de planetas...
                        //En caso de tener BBDD, los iría incluyendo en ella de forma automática para futuras peticiones...
                        //if (!Utilidades.lstPlatenasDisponibles.Contains(approach_data["orbiting_body"].ToString().ToLower()))
                        //{
                        //    HttpContext.Current.Response.Write("orbiting_body: " + approach_data["orbiting_body"].ToString().ToLower());
                        //    HttpContext.Current.Response.End();
                        //}

                        //Comprobamos si tienen está el planeta en la aproximación...
                        if (approach_data["orbiting_body"].ToString().ToLower() == cPlanet)
                        {
                            Asteroide cAsteroide = new Asteroide();

                            //Establecemos los valores...
                            cAsteroide.Nombre = asteroide["name"].ToString();
                            cAsteroide.Diametro = (Convert.ToDouble(asteroide["estimated_diameter"]["kilometers"]["estimated_diameter_min"]) + Convert.ToDouble(asteroide["estimated_diameter"]["kilometers"]["estimated_diameter_max"]) / 2);
                            cAsteroide.Velocidad = approach_data["relative_velocity"]["kilometers_per_hour"].ToString();
                            cAsteroide.Fecha = Convert.ToDateTime(Convert.ToDateTime(approach_data["close_approach_date"].ToString()).ToShortDateString());
                            cAsteroide.Planeta = approach_data["orbiting_body"].ToString();

                            //Añadimos el asteroide a la lista...
                            LstAsteroides.Add(cAsteroide);
                            break;
                        }
                    }
                }
            }

            if (joResponse["links"]["next"] == null )//|| joResponse["links"]["next"].ToString() == "http://www.neowsapp.com/rest/v1/neo/browse?page=10&size=20&api_key=zdUP8ElJv1cehFM0rsZVSQN7uBVxlDnu4diHlLSb")
                break;
            else
                //Realizamos la petición con la siguiente página...
                joResponse = obtenerDatosPeticion(joResponse["links"]["next"].ToString());
        }


        //Comprobamos si hemos recuperado asteroides...
        if (LstAsteroides.Count > 0)
        {
            //Ordenamos los asteroides...
            LstAsteroides.Sort((x, y) => y.Diametro.CompareTo(x.Diametro));
            List<Asteroide> lstTop3 = LstAsteroides.Take(3).ToList();
                
            //Establecemos el mensaje de alerta...
            cResult = new Result(1, "Listado recuperado.", lstTop3);
        }
        else
            //Establecemos el mensaje de alerta...
            cResult = new Result(-3, "Ningún asteroide para ese planeta.");
    }

    public JObject obtenerDatosPeticion(string cConsulta)
    {
        JObject joResponse = null;
        var client = new RestClient(cConsulta);
        var request = new RestRequest();
        
        //Configuramos la petición...
        request.Method = Method.GET;
        request.RequestFormat = DataFormat.Json;
        request.AddHeader("Accept", "application/json");
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        IRestResponse response = client.Execute(request);
        
        //Comprobamo si la petición se ha realizado correctamente...
        if (response.StatusCode == System.Net.HttpStatusCode.OK)
            //Parseamos el resultado...
            joResponse = JObject.Parse(response.Content.ToString());

        //Devolvemos lo recuperado...
        return joResponse;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}