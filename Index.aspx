﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" CodeFile="Index.aspx.cs" Inherits="Index" Theme="Nasa" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="containerHome">
        <div>
            <div class="row mtForm">
                <div class="col-md pcolForm">
                    <div class="row bottomVertical">
                        <div>
                            <label>Planeta: </label>
                        </div>
                        <div class="col-md">
                            <asp:DropDownList ID="DDLPlatena" ClientIDMode="Static" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-md pcolForm">
                    <div class="row bottomVertical">
                        <div class="col-md">
                            <div class="btnPrimary" data-elemento="btn-obtener-asteroides">OBTENER ASTEROIDES</div>
                        </div>
                    </div>

                </div>
            </div>
            <div data-elemento="box-listado">

            </div>
        </div>
    </div>
</asp:Content>

