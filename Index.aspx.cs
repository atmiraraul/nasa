﻿using System.Web.UI;

public partial class Index : BasePage
{
    private void Page_Init(object sender, System.EventArgs e)
    {
    }

    private void Page_Load(object sender, System.EventArgs e)
    {
        //Comprobamos si es postback...
        if (!Page.IsPostBack)
            configurarPagina();
    }

    public void configurarPagina()
    {
        //Lincamos los planetas permitidos...
        DDLPlatena.DataSource = Utilidades.lstPlatenasDisponibles;
        DDLPlatena.DataBind();
    }
}