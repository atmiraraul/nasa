﻿using System;
using System.Web.UI;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            configurarPagina();
    }

    public void configurarPagina()
    {
        //Establecemos la ruta...
        HFUrlBaseAjax.Value = Utilidades.nombreDominio();

        //Establecemos el código...
        PHLOCAL.Visible = Utilidades.servidorDesarrollo();
        PHPRO.Visible = !Utilidades.servidorDesarrollo();
    }
}
