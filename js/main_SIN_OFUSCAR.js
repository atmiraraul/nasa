var bQuitarOverlay = true;
$(document).ready(function () {

    $("[data-elemento='btn-obtener-asteroides']").click(function () {
        mostrarOcultarCargando(true);

        $.ajax({
            type: "GET",
            url: "/interfaces/iNasa.ashx?planet=" + $("#DDLPlatena").val() + "&opcional=" + generarCadenaAleatoria(),
            success: function (resultado) {

                if (resultado.coderesult <= 0) {
                    //Mostramos el mensaje...
                    mostrarAlerta(resultado.cMessage, "OK");
                    mostrarOcultarCargando(false);
                }
                else {
                    //Mostramos el mensaje...
                    mostrarAlerta(resultado.cMessage, "OK");

                    //Limpiamos el listado...
                    $("[data-elemento='box-listado']").empty();

                    //Recorremos el listado...
                    for (var i = 0; i < resultado.lstAsterioides.length; i++)
                        //Lo añadimos al box...
                        $("[data-elemento='box-listado']").append("<br/>-Nombre:" + resultado.lstAsterioides[i].Nombre + ",  diámetro: " + resultado.lstAsterioides[i].Diametro + " y  velocidad: " + resultado.lstAsterioides[i].Velocidad);

                    //Ocultamos el cargando...
                    mostrarOcultarCargando(false);
                }

            }
        });
    });

    $("#LMensaje,[data-elemento='mensaje-alerta'],.overlay").click(function () {
        ocultarAlerta();
    });

    $(".overlayMail,[data-elemento='cerrar-modal']").click(function () {
        ocultarFormularioEmail();
    });

});



jQuery(window).load(function () {
    //Cuando haya finalizamos ocultamos el cargando...
    mostrarOcultarCargando(false);
});

function comprobarMensajeAlerta()
{
    //Comprobamos si hay algun mensaje...
    if ($("#HFMensajeAlerta").val() != "") {
        bQuitarOverlay = false;
        //Establecemos los valores...
        $("#LMensaje [data-tipo='mensaje']").html($("#HFMensajeAlerta").val());
        $("#HFMensajeAlerta").val("");
        mostrarAlerta();
    }
}

function mostrarVentanaAlerta(Mensaje, TipoAlerta) {
    bQuitarOverlay = false;
    
    var BoxAlert = $("[data-tipo='box-alert']");
    //Ocultamos el box...
    BoxAlert.hide();

    //Configuramos los botones...
    $("[data-tipo-btn-alert]").hide();
    $("[data-tipo-btn-alert='" + TipoAlerta + "']").show();

    //Limpiamos el mensaje...
    BoxAlert.find(".mensaje").html("");
    BoxAlert.find(".mensaje").html(Mensaje);

    //Mostramos el mensaje...
    BoxAlert.fadeIn("slow");
    $(".overlay").show();
}

function mostrarAlerta(cMensaje) {
    //Asignamos el mensaje...
    if (cMensaje != undefined && cMensaje != "")
        $("[data-elemento='mensaje-alerta'] [data-tipo='mensaje']").html(cMensaje);

    //$("[data-elemento='mensaje-alerta']").fadeIn("slow");
    $("[data-elemento='mensaje-alerta']").fadeIn();
    $(".overlay").fadeIn();
    setTimeout('ocultarAlerta();', 5000);
}

function ocultarAlerta() {
    $("[data-elemento='mensaje-alerta']").fadeOut("slow", function () {
        $(".overlay").fadeOut();
        $(".overlay-registrado").hide();
	});
}
function ocultarAlertaEspecial() {
    $("[data-elemento='mensaje-alerta-especial']").fadeOut("slow", function () {
      
        $(".overlay-registrado").hide();
    });
}


function generarCadenaAleatoria() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';

    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }

    //Devolvemos el resultado...
    return randomstring;
}

function mostrarOcultarCargando(mostrar) {
    if (mostrar) {
        $('#divLoading').fadeIn(1000);
    }
    else {  
        /*muestro*/
       $('#divLoading').fadeOut(1000);

       comprobarMensajeAlerta(); 
    }
}